package com.example.parcel.api;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import com.example.parcel.api.dto.VoucherAPIResponse;

@Component
public class VoucherAPIService {

	public VoucherAPIResponse getDiscount(String voucher) {
		RestTemplate rt = new RestTemplate();
		String uri = "https://mynt-exam.mocklab.io/voucher/"+ voucher + "?key=apikey";
    	VoucherAPIResponse result = rt.getForObject(uri, VoucherAPIResponse.class);
		return result;
		
	}
	
	
}
