package com.example.parcel.rules.config;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.FileCopyUtils;

@Configuration
//@ConfigurationProperties(prefix = "myapp")
@PropertySource("classpath:rules.json")
public class RulesConfiguration {
	private int priority;
	private String ruleName;
	private String condition;
	private String cost;
	
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public String getRuleName() {
		return ruleName;
	}
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}

    public String loadJsonStringFromClasspathResource(String filename) throws IOException {
        ClassPathResource resource = new ClassPathResource(filename);
        byte[] fileBytes = FileCopyUtils.copyToByteArray(resource.getInputStream());
        return new String(fileBytes, StandardCharsets.UTF_8);
    }
    
	
	
}
