package com.example.parcel.rest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.parcel.api.VoucherAPIService;
import com.example.parcel.api.dto.VoucherAPIResponse;
import com.example.parcel.dto.ParcelResponse;
import com.example.parcel.params.ParcelParam;
import com.example.parcel.rules.config.RulesConfiguration;


@RestController
@RequestMapping("/parcel")
public class ParcelRestController {
	
    @Autowired
    private VoucherAPIService voucherAPI;
    
    @Autowired
    private RulesConfiguration rulesConfig;
       
    @GetMapping
    public String healthCheck() {
        // Implement
    	return "Hello World!";
    }

    @PostMapping
    public ParcelResponse getParcel(@Validated @RequestBody ParcelParam parcel, String voucherCode) {
        // Implement
    	
    	ParcelResponse parcelCost;
    	
    	try {
    		String rules = rulesConfig.loadJsonStringFromClasspathResource("rules.json");
    		JSONObject obj =  new JSONObject(rules);
    		JSONArray arr = obj.getJSONArray("rules");
    		
    		double volume = parcel.getLength() * parcel.getWidth() * parcel.getHeight();
        	double cost = 0;
        
        	if (parcel.getWeight() > arr.getJSONObject(0).getJSONObject("condition").getInt("value")) {
        		
        		parcelCost = new ParcelResponse(1,
        				arr.getJSONObject(0).getString("ruleName"), 
        				arr.getJSONObject(0).getJSONObject("condition").getString("definition"), 
        				arr.getJSONObject(0).getString("cost"));
    			
        	}else if (parcel.getWeight() > arr.getJSONObject(1).getJSONObject("condition").getInt("value")){
        		cost = arr.getJSONObject(1).getJSONObject("cost").getDouble("value") * parcel.getWeight();
        		parcelCost = new ParcelResponse(2, 
        				arr.getJSONObject(1).getString("ruleName"), 
        				arr.getJSONObject(1).getJSONObject("condition").getString("definition"), 
        				"PHP " + cost);
    			
        	}else if (volume < arr.getJSONObject(2).getJSONObject("condition").getInt("value")){
        		cost = arr.getJSONObject(2).getJSONObject("cost").getDouble("value") * volume;
        		parcelCost = new ParcelResponse(3, 
        				arr.getJSONObject(2).getString("ruleName"), 
        				arr.getJSONObject(2).getJSONObject("condition").getString("definition"), 
        				"PHP " + cost);
    		
        	}else if (volume < arr.getJSONObject(3).getJSONObject("condition").getInt("value")){
        		cost = arr.getJSONObject(3).getJSONObject("cost").getDouble("value") * volume;
        		parcelCost = new ParcelResponse(4, 
        				arr.getJSONObject(3).getString("ruleName"), 
        				arr.getJSONObject(3).getJSONObject("condition").getString("definition"), 
        				"PHP " + cost);
    			 
        	}else {
        		cost = arr.getJSONObject(4).getJSONObject("cost").getDouble("value") * volume;
        		parcelCost = new ParcelResponse(5, 
        				arr.getJSONObject(4).getString("ruleName"), 
        				arr.getJSONObject(4).getJSONObject("condition").getString("definition"), 
        				"PHP " + cost);
        	}
        	
        	if (voucherCode != null) {
        		VoucherAPIResponse voucherResponse = voucherAPI.getDiscount(voucherCode);
        		double discountAmount = (voucherResponse.getDiscount() / cost) * 100;
        		if (cost != 0) {
        		cost = cost - discountAmount;
        		parcelCost.setCost("PHP " + cost);	
        		}
        	}
     
        	return parcelCost;
    		
    		
    	} catch (Exception e) {
    		return null;
    	}
    		
    }
}