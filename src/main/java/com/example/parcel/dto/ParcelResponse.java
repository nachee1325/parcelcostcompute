package com.example.parcel.dto;

public class ParcelResponse {
	private int priority;
	private String ruleName;
	private String condition;
	private String cost;
	
	public ParcelResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ParcelResponse(int priority, String ruleName, String condition, String cost) {
		super();
		this.priority = priority;
		this.ruleName = ruleName;
		this.condition = condition;
		this.cost = cost;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public String getRuleName() {
		return ruleName;
	}
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
		
}
