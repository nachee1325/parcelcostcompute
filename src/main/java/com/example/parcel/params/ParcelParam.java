package com.example.parcel.params;
//@Entity
//@Table(name = "user")
public class ParcelParam {
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
	private long weight;
	private long height;
	private long width;
	private long length;
	public long getWeight() {
		return weight;
	}
	public void setWeight(long weight) {
		this.weight = weight;
	}
	public long getHeight() {
		return height;
	}
	public void setHeight(long height) {
		this.height = height;
	}
	public long getWidth() {
		return width;
	}
	public void setWidth(long width) {
		this.width = width;
	}
	public long getLength() {
		return length;
	}
	public void setLength(long length) {
		this.length = length;
	}
	
}