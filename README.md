# parcelcostcompute



## Getting started

Things needed to run:

1. Java 8+
2. Maven

- Clone this repo on your local and clean install dependencies:
```
git clone https://gitlab.com/nachee1325/parcelcostcompute
mvn clean install
```

## How to use?

- Run the application, it should run on localhost:8080
```
cd parcelcostcompute
mvn spring-boot:run
```
- For health check
```
curl http://localhost:8080/parcel
```

- For regular parcel cost calculation (sample only)
```
curl --request POST http://localhost:8080/parcel --header 'Content-Type: application/json' \
--data-raw '{
    "weight": 51.0,
    "height": 11.0,
    "width": 12.0,
    "length": 12.0
}'
```

- For parcel cost calculation with voucher (sample only)
```
curl --request POST http://localhost:8080/parcel?voucherCode=MYNT --header 'Content-Type: application/json' \
--data-raw '{
    "weight": 51.0,
    "height": 11.0,
    "width": 12.0,
    "length": 12.0
}'
```

## Updating Rule Configuration

- Go to project >  src > main> resources > rules.json
- We can change the condition and cost values depending on the market fluctuations in terms of pricing 
```
{
	"rules": [
		{
			"priority": "1",
			"ruleName": "Reject",
			"condition": {"definition":"Weight Exceeds 50kg", "value": 50},
			"cost": "N/A"
		},
		{
			"priority": "2",
			"ruleName": "Heavy Parcel",
			"condition":  {"definition":"Weight Exceeds 10kg", "value": 10},
			"cost":  {"multiplicand":"weight", "value": 10}
		},
		{
			"priority": "3",
			"ruleName": "Small Parcel",
			"condition": {"definition": "Volume is less than 1500 cubic cm", "value": 1500},
			"cost": {"multiplicand":"volume", "value": 0.03}
		},
		{
			"priority": "4",
			"ruleName": "Medium Parcel",
			"condition": {"definition": "Volume is less than 2500 cubic cm", "value": 2500},
			"cost": {"multiplicand":"volume", "value": 0.04}
		},
		{
			"priority": "5",
			"ruleName": "Large Parcel",
			"condition": "",
			"cost": {"multiplicand":"volume", "value": 0.05}
		}
	]
}
``` 

